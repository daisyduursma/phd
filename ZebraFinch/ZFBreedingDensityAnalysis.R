#working directory
dat.dir<-'/Users/daisy/Google Drive/PhD/Data/Observaitons/Cleaned/Breeding/'



#=====================================
#Extract biome summary for ZF
#=====================================
spSummary<-read.csv('/Users/daisy/Google Drive/PhD/BreedingTiming/tables/BreedingQuantiles2015-06-11.csv')
ZFsub<-subset(spSummary,CommonName=="Zebra Finch")
write.csv(ZFsub,'/Users/daisy/Google Drive/PhD/ZebraFinch/Tables/ZebraFinchEcoRegion20150617.csv',row.names = FALSE)


# ======================================
# circular plot of Australia with density curves for biomes
# ===================================== 
#read in observations
dat<-read.csv(paste0(dat.dir,'PointOfLayDayOfYear2015-05-26.csv'))
ZFdat<-subset(dat, Scientific.Name=="Taeniopygia guttata")
ZFdat$Radians <-(ZFdat$DOY_PL/366*360)*pi / 180
koeppen<-raster(paste0('/Users/daisy/Google Drive/PhD/Data/Spatial/BOM_climate_zones/kpngrp_major/koepenReclassified.asc'),
                proj4string=CRS("+proj=longlat +datum=WGS84 +ellps=WGS84"))
ZFdat$koeppen<-extract(koeppen,data.frame(cbind(ZFdat$lon, ZFdat$lat)))

par(mar=c(1, 1, 0, 10))
PLc<- circular(ZFdat$Radians, units = "radians", zero=pi/2, 
               rotation = "clock")

#get midpoints of 12 months on circle
bins<-12
arc <- (2 * pi)/bins
sector <- seq(0, 2 * pi - (2 * pi)/bins, length = bins)
midpoints <- circular(seq(arc/2, 2 * pi - pi/bins, length = bins), units = "radians", zero=pi/2, 
                      rotation = "clock")

#plot(PLc, cex=1.1, bin=720, stack=TRUE, sep=0.035, shrink=1.8)
rose.diag(PLc, bins=12, col="darkgrey", cex=1.1, prop=1.7,axes = FALSE,ticks = FALSE, shrink=1.5)
#rose.diag(PLc, bins=12, col="darkgrey", cex=1.1, prop=1.3,rotation='clock',zero=pi/2,axes = FALSE,ticks = FALSE, shrink=1.8)
axis.circular(at=midpoints, labels=c(1:12), cex=1.1,tcl=0.075)
#ticks.circular(midpoints, rotation='clock', tcl=0.075)
#lines(density.circular(PLc, bw=40), lwd=2, lty=1)#Australia Density

koepClass<-c(32,22,13,3)

Biome<-c("Subtropical","Desert","Grassland","Temperate")
kpColour<-c("chartreuse4", "chocolate4","darkgoldenrod1","cornflowerblue")

for (k in 1:length(koepClass)){
  kpdat<-subset(ZFdat,koeppen==koepClass[k])
  if (nrow(kpdat)>49){
    #kpdat<- kpdat[!duplicated(kpdat[c("Scientific.Name","MOY_PL")]),]
    kpdat<-circular(kpdat$Radians, units = "radians", zero=pi/2, 
                    rotation = "clock")
    lines(density.circular(kpdat, bw=12), lwd=3, lty=k,col = kpColour[k])
    arrows.circular(mean(kpdat), lwd=3, lty=k,col = kpColour[k])
  }
}

legend("topright", legend=Biome, pch=1 , lwd=1,col=kpColour, bty="n", text.font=3)


#=======================================================
#analysis with temperture and precip for each biome
#=======================================================
#climate variabls
clim<-c('prec','tmax','tmin' )
climateDir<-"/Users/daisy/Google Drive/PhD/Data/Spatial/Climate/EMASTClimate_mmn"

#koeppen zones
koeppen<-raster(paste0('/Users/daisy/Google Drive/PhD/Data/Spatial/BOM_climate_zones/kpngrp_major/koepenReclassified.asc'),
                proj4string=CRS("+proj=longlat +datum=WGS84 +ellps=WGS84"))
r1 <- raster(paste0(climateDir,"/",clim[1],"/",files[1]))
#make koeppen zones (more course than climate) so that they have same
#resolution and extent as climate
kp2<-resample(koeppen,r1,method="ngb")

#ZF observations
dat<-read.csv(paste0(dat.dir,'PointOfLayDayOfYear2015-05-26.csv'))
ZFdat<-subset(dat, Scientific.Name=="Taeniopygia guttata")
ZFdat$koeppen<-extract(koeppen,data.frame(cbind(ZFdat$lon, ZFdat$lat)))
Locs <- SpatialPoints(cbind(ZFdat$lon, ZFdat$lat),
                        proj4string = CRS('+proj=longlat +datum=WGS84'))

#climatic conditions for biomes
endClimDat<-list()
for (c in 1:length(clim)) {
  #get climate files
  files <- list.files(paste0(climateDir,"/",clim[c]))
  # make a loop to sort through the files
  climdat<-list()
  for (i in 1:length(files)) {
    # make a raster for the desired timeperiod
    r1 <- raster(paste0(climateDir,"/",clim[c],"/",files[i]))
    month<-strsplit(strsplit(files[i],"_")[[1]][6],".nc")[[1]][1]
     # extract the data and write it directly to the csv file
    MeanBiomes <- zonal(r1,kp2, fun = mean,na.rm=TRUE)
    sdBiomes <- zonal(r1,kp2, fun = sd,na.rm=TRUE)
    # reasign the column name to the one we generated
    enddat<-merge(MeanBiomes,sdBiomes, by="zone")
    colnames(enddat)<-c("Biome",paste0(clim[c],"Mean"),paste0(clim[c],"Sd"))
    enddat$Month<-month
    climdat[[i]]<-enddat
  }
 endClimDat[[c]]<-data.frame(do.call("smartbind",climdat))
}
#make table of summary  
final<-do.call("cbind",endClimDat)[c("Biome","Month","precMean","precSd","tmaxMean","tmaxSd",
                                     "tminMean","tminSd")]

#make 2X2 graph of breeding density in biome and precip and temp
par(mfrow=c(2,2), mar = c(3,3,2,3))
koepClass<-c(32,22,13,3)
Biome<-c("Subtropical","Desert","Grassland","Temperate")
kpColour<-c("chartreuse4", "chocolate4","darkgoldenrod1","cornflowerblue")

for (k in 1:length(koepClass)){
  #plot tmin, tmax, and precip for biome
  tmin<-subset(final,Biome==koepClass[k])[c("tminMean","Month")]
  tmax<-subset(final,Biome==koepClass[k])[c("tmaxMean","Month")]
  prec<-subset(final,Biome==koepClass[k])[c("precMean","Month")]
  
  plot(tmax$Month, tmax$tmaxMean,xlab="",ylab = "",ylim = c(10,40),
       main = Biome[k],type = "l",col="red")
  mtext(substitute(paste(Temperature, B * degree, "C)"), list(B = " (")),
        side=2, line=2,cex=.8)
  mtext('Month', side=1, line=2,cex=.8)
  par(new = TRUE)
  plot(prec$Month,prec$precMean,type = "l", axes = FALSE, bty = "n",
       xlab = "", ylab = "",col = "blue",ylim=c(5,110))
  axis(side=4, at = pretty(5:100))
  mtext("Prec", side=4, line=2,cex=.8)
  kpdat<-subset(ZFdat,koeppen==koepClass[k],select=DOY_PL)
  par(new = TRUE)
  hist(kpdat[,1],main="",xlab="",ylab = "",xlim = c(1,365),axes=FALSE,freq=FALSE,breaks=seq(1,366,length = 13))
} 

#legend("topright", legend=Biome, pch=1 , lwd=1,col=kpColour, bty="n", text.font=3)




