#make sure workspace is clean
rm(list = ls())

#library(raster)
library(maptools)
library(raster)

#read in biomes with correct corrdinate system
biomes<-readShapePoly("/Users/daisy/Google Drive/PhD/Data/Spatial/Australian_Ecoregions/Bioregions_biomes.shp",proj4string=CRS("+proj=longlat +datum=WGS84 +ellps=WGS84"))

plot(biomes, axes=TRUE, col=terrain.colors(7))

#explore the data
head(biomes@data)
print(proj4string(biomes))

#make table with climatic variables 

clim<-list.files("/Users/daisy/Documents/data/climate_mmn",recursive = TRUE,full.names=TRUE)

#make dataframe of Biomes names
datBiomes<-as.data.frame(biomes)["Biome_name"]
#loop to extract the data and add summary data about each variable
for (i in 1:length(clim)) {
    # read in raster
    r1 <- raster(clim[i],band = 1,proj4string=CRS("+proj=longlat +datum=WGS84 +ellps=WGS84"))
    # extract the data for each biome
    r1extract<- extract(r1, biomes)
    #get mean and sd for each biome and rename columns in dataframe
    datBiomes$r1mean<-unlist(lapply(r1extract, function(x) {
      if (!is.null(x)) mean(x, na.rm=TRUE) else NA
    }))
    name<-strsplit(clim[i],"/")[[1]][8]
    colnames(datBiomes)[ncol(datBiomes)] <- paste0(name,"_mean")
    
    datBiomes$r1sd<-unlist(lapply(r1extract, function(x) {
      if (!is.null(x)) sd(x, na.rm=TRUE) else NA
    }))
    name<-strsplit(clim[i],"/")[[1]][8]
    colnames(datBiomes)[ncol(datBiomes)] <- paste0(name,"_sd")
}

write.csv(datBiomes, paste0("/Users/daisy/Google Drive/PhD/Nesting_habitats/tables/biomeClimateSummary", as.Date(Sys.time()),".csv"),row.names = FALSE)
