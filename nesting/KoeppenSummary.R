rm(list = ls())
library(maptools)
library(raster)


dat<-c("/Users/daisy/Google Drive/PhD/Nesting_habitats/data/kpn_fine/fineAus_01deg.asc","/Users/daisy/Google Drive/PhD/Nesting_habitats/data/kpn_major/majorAus_01deg.asc")

dat_name<-c("fineKPN","majorKPN")

clim<-list.files("/Users/daisy/Documents/data/climate_mmn",recursive = TRUE,full.names=TRUE)

for (i in 1:length(dat)){
  kpZones<-raster(dat[i],crs =("+proj=longlat +datum=WGS84 +ellps=WGS84"))
  zone<-unique(kpZones)
  zonalSummary<-data.frame(zone = zone)
  colnames(zonalSummary)<-"zone"
  
  #loop to extract the data and add summary data about each variable
  for (ii in 1:length(clim)) {
      # read in raster
      r1 <- raster(clim[ii],band = 1,proj4string=CRS("+proj=longlat +datum=WGS84 +ellps=WGS84"))
      name<-strsplit(clim[ii],"/")[[1]][8]
      #get mean and sd for each biome and rename columns in dataframe
      z.mean <- zonal(r1, kpZones, mean, na.rm = T)
      zonalSummary<- merge(zonalSummary,z.mean,by.all="zone")
      colnames(zonalSummary)[ncol(zonalSummary)] <- paste0(name,"_mean")
      z.sd <- zonal(r1, kpZones, sd, na.rm = T)
      zonalSummary<- merge(zonalSummary,z.sd,by.all="zone")
      colnames(zonalSummary)[ncol(zonalSummary)] <- paste0(name,"_sd")
  }
  write.csv(zonalSummary, paste0("/Users/daisy/Google Drive/PhD/Nesting_habitats/tables/",dat_name[i],"ClimateSummary", as.Date(Sys.time()),".csv"),row.names = FALSE)
}
