rm(list = ls())

library(stringr)
library(Hmisc)

#get species interested and their BirdLife ID number
#spdat<-read.csv("/Users/daisy/Google Drive/PhD/Data/CockburnCommunalBreeding/CockburnGarnett_2015-01-12.csv")
garnett<-subset(read.csv("/Users/daisy/Google Drive/PhD/birdTraits/Garnett paper/NSD-Data Descriptor/Australian_Bird_Data_Version_1_0.csv"), Species ==1)
spdat<-garnett[c("Taxon.scientific.name","BirdLife.Australia.Species.no.")]
garnett$Taxon.scientific.name<- str_trim(garnett$Taxon.scientific.name)
garnett$Taxon.scientific.name<-capitalize(tolower(garnett$Taxon.scientific.name))
garnett<- subset(garnett,Population.description == "Endemic (breeding only)"# | Population.description == "Australian" 
                 | Population.description == "Endemic (entirely Australian)")
garnett<- subset(garnett, Australian.Capital.Territory == "Core" | New.South.Wales == "Core" |	Northern.Territory == "Core" |	Queensland == "Core" |	South.Australia == "Core" |	Tasmania == "Core" |	Victoria == "Core" |	Western.Australia == "Core")

spdat<-garnett[c("Taxon.scientific.name","BirdLife.Australia.Species.no.")]
spdat$SpNo<-garnett$BirdLife.Australia.Species.no.
SpNo<-garnett$BirdLife.Australia.Species.no.
#Taxon<-read.csv("/Users/daisy/Google Drive/PhD/birdTraits/Data for Megan/TaxonList.csv")[,c("SpNo"        ,"TaxonSciName")]
#species<-merge(spdat,Taxon,by.x ="Taxon.scientific.name", by.y = "TaxonSciName",all.x=TRUE)
#garnett<-subset(read.csv("/Users/daisy/Google Drive/PhD/birdTraits/Garnett paper/NSD-Data Descriptor/Australian_Bird_Data_Version_1_0.csv"), Species ==1)
#garnett$Taxon.scientific.name<- str_trim(garnett$Taxon.scientific.name)
#garnett$Taxon.scientific.name<-capitalize(tolower(garnett$Taxon.scientific.name))
#species<- spdat[,c("Taxon.scientific.name","Taxon.sort")]
#NRS - taxon.sort - SPECIES

#read in NRS data and BL Atlas data
nesting<-read.csv("/Users/daisy/Google Drive/PhD/Data/Observaitons/Raw/BirdLife/BLA_NRS/NRSExtract.csv")[,c("SPECIES","Lat","Lon","YEAR")]
colnames(nesting)<-c("SpNo","lat","lon","year")
nesting$sourceName<-"BA - Nest Record Scheme"
nesting<-subset(nesting, !is.na(lat) & !is.na(lon))

#read in BLA data, currently missing month and year
BLA<-read.csv("/Users/daisy/Google Drive/PhD/Data/Observaitons/Raw/BirdLife/BLA data.csv")



/Users/daisy/Dropbox/Data for Megan
BLA<-read.csv("/Users/daisy/Dropbox/Data for Megan/BLA data.csv")
#get only breeding obse
BLA2<-subset(BLA, brRnge==1, select=c("SpNo","Latitude","Longitude","Source_Name"))
colnames(BLA2)<-c("SpNo","lat","lon","sourceName")
#add fake year
BLA2$year <- 1000


#read in egg data and resolve the names
eggs<-read.csv("~/Users/daisy/Google Drive/PhD/Data/Observaitons/Raw/Eggs/egg data/egg data13012015.csv")
eggs<-subset(eggs, Coordinates.are.out.of.range.for.species != "true" & species.Outside.Expert.Range != "true")
eggs<-eggs[c("Scientific.Name","Vernacular.Name","Matched.Scientific.Name","Species...matched","Collection.Code","Latitude...processed","Longitude...processed","Year...parsed","Month...parsed")]
colnames(eggs)<-c("scn1","common_name","scn2","scn3","Collection.Code","lat", "lon","year","month")
eggs<-subset(eggs, !is.na(lat) & !is.na(lon))
eggs$scn1<- str_trim(eggs$scn1)
eggs$scn1<-capitalize(tolower(eggs$scn1))
#update species names in eggs 
eggres<-subset(read.csv("/Users/daisy/Google Drive/PhD/Data/Observaitons/Raw/Eggs/egg data/namesResolved.csv"),is.na(unwanted))

eggfix<-eggs[eggs$scn1 %in% eggres$scn1,]
eggfix<-merge(eggfix,eggres, by ="scn1")
eggfix$scn1<-eggfix$taxon_name
#get obs that did not need to be updated
eggfine<-subset(eggs, !(scn1 %in% eggres$scn1))
#put the egg obs back together
eggfixed<-rbind(eggfine[c("scn1","lat","lon","year")],eggfix[c("scn1","lat","lon","year")])
eggfixed$sourceName<-"ALA_EGGS"
#remove year, month duplications
eggfixed<-eggfixed[!duplicated(eggfixed),]
write.csv(eggfixed,"/Users/daisy/Google Drive/PhD/Data/Observaitons/Cleaned/ALA_eggdata.csv",row.names=FALSE)
#add the species number.
eggfixed<-merge(eggfixed,spdat,by.x = "scn1",by.y = "Taxon.scientific.name")
colnames(eggfixed)<-c("scn1","lat", "lon","year","sourceName","SpNo")
eggfixed<-eggfixed[c("SpNo","lat","lon","year","sourceName")]

#combine datasets

obs<-rbind(nesting,BLA2,eggfixed)
#select out species interested in
obs<-obs[obs$SpNo %in% SpNo,]  
#remove duplicates
obs<- obs[!duplicated(obs[,c("SpNo","lat","lon","year")]), ]
#unique locaitons
obsLocs<- obs[!duplicated(obs[,c("SpNo","lat","lon")]), ]

yearlyObs<-as.data.frame(table(obs$SpNo))
LocsObs<-as.data.frame(table(obsLocs$SpNo))

spdat2<-merge(spdat,yearlyObs, by.x ="SpNo", by.y = "Var1", all.x=TRUE)
colnames(spdat2)[4]<-"yearlyObsCount"
spdat2<-merge(spdat2,LocsObs, by.x ="SpNo", by.y = "Var1", all.x=TRUE)
colnames(spdat2)[5]<-"UniqueLocsCount"
#replace the NA with 0 becaue there are 0 observations
spdat2[is.na(spdat2)] <- 0

write.csv(obsLocs,paste0("/Users/daisy/Google Drive/PhD/Data/Observaitons/Cleaned/BreedingEndemics_", as.Date(Sys.time()),".csv"),row.names=FALSE)

write.csv(spdat2,paste0("/Users/daisy/Google Drive/PhD/Nesting/tables/EndemicBreedingSummary_", as.Date(Sys.time()),".csv"),row.names=FALSE)





